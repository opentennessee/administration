# Open Tennessee Safe Volunteer Deliveries Project Manager Contract

## The Project: Safe Volunteer Deliveries
In the midst of the COVID-19 pandemic, many nonprofits have to deliver goods and services differently to maintain social distancing.
Open Tennessee is working with Code for Nashville to reach out to nonprofits and help improve their practices and tooling to more efficiently coordinate their volunteers to make these deliveries safely.

The project received sponsorship from Metro Humans Relations Commission which allows us to provide honorariums for the amazing volunteers who are leading it.

## Parties
- "The Contractor" or "The Project Manager": Brittany City
- "The Company": Open Tennessee

## Initial Dates
The contract will initially be effective for July and August of 2020. 
It may be renewed or replaced at any time with signed amendments.

## Compensation
The Contractor will receive $500 per month of the contract as an honorarium.
The Company will make payments at least once a month and (considering this not market rate compensation) before the respective month is completed.
Either party may terminate the agreement at any time, in which case, the Company may prorate any pending compensation to match the portion of the month served.

## Statement of Work
The Project Manager will:
1. Share a schedule of at least 5 hours per week when they will typically be available for meetings etc
1. Budget at least 5 additional 5 hours per week (which do not need to be scheduled or publicized) for work between meetings
1. Reply to client and Company emails within 72 hours except (or make other arrangements) 
1. Join all meetings by 5 minutes after official start or communicate in advance, cancelling if necessary
1. Within first week, reach out to 2-4 nonprofit clients
1. Within second week, plan realistic project roadmap (to refine over the course of the project), likely to include:
    1. Observe and improve a nonprofit's current processes
    1. Research, test, and deploy third party software solutions with the nonprofit as appropriate
1. Help interview and select additional honorarium candidates as necessary
1. Recruit and coordinate volunteers and honoree(s) to fulfill goals
1. Establish a project management process to publicly track progress, opportunities, and blockers
1. Communicate with Open Tennessee leadership at least once a week
1. Track their time to help us estimate future work for grants etc
1. Protect any personal or proprietary information of our nonprofit clients, their volunteers, or their clients except where they have consented to making it open\*
1. Forfeit any right to control how the Company modifies, distributes, or licenses the Contractor's work product on this project except where other specific agreements are made in writing\*\*

\*Open Tennessee's goal is to make systems and institutions more open and accessible to the people they impact.
However, there are valid reasons not to disclose some information such as personal information.
Even in cases where we are trying to convert an organization to be more open with certain information, we choose to treat what they've closed as closed so we can safely discuss what it would be like to open it.
If they ultimately decide to keep it closed then we can walk away leaving the relationship intact in case a better opportunity comes along.

\*\*Open Tennessee will not amass closed intellectual property or steal intellectual property from anyone including its collaborators, volunteers, or associates.
Everything we do (paid or not) is intended for public consumption and we believe in giving credit to the individuals who did the work.
So, we intend this clause to make both parties equally incapable of limiting the other in their use of the work product.

## Support
Open Tennessee will provide:
- a Code for Nashville email address
- mentoring and advice
- recruiting others to help
- connections to other organizations

## This Agreement
### Severability
Nothing in this agreement is intended to violate the law.
Each clause should be interpreted independently.
If any clause is unenforceable, it should be given an enforceable interpretation that matches its original intent as best as possible.
If there is no interpretation that leaves it enforceable, it shall be stricken while leaving the rest of the contract intact as much as is permissible by the law.

### Prioritizing Mediation over Litigation
Open Tennessee does not write contracts to trick or trap anyone.
Both parties commit that if at any point they feel unsatisfied with the other party's cooperation relating to this agreement, they will attempt to coordinate a third party to mediate before filing suit.
Attempting to coordinate a third party to mediate includes a written commitment from that third party to conduct that mediation (pending the consent of the other party), a written request to the other party to accept the third party mediator, and a reasonable amount of time for the other party to respond.
The second party may conduct the same process in response to suggest a different third party to mediate.
Neither party will file suit against the other in relation to this agreement unless they have followed the above and they could not agree on an eligible third party to mediate, or the other party refused or did not respond within a reasonable amount of time.

## Signatures
### The Contractor
Business Name (or personal name if the same):

Date:

Signature:

### The Company
Name:

Title:

Date:

Signature:
