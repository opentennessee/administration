# Bylaws of Open Tennessee
## Definitions
### Consent
Consent is reached for a proposal when all members affirm they believe it is "good enough to start and safe enough to try".

In order to pass, at least one member (a Champion) must commit to ensuring the proposal is effectively and responsibly carried out. 
Other members may consent to a proposal without any commitment or expectation that they will actively support its execution.
Even in consenting to a proposal, members retain their right to make other proposals later that may reverse it.
Members always have the right to have a statement of counter arguments, concerns, or considerations included in the minutes with a proposal (regardless of whether they ultimately consent to the proposal).
However, any member who consents commits not to work against or undermine the proposal while it is active.
If any set of members hold any exclusive duties such as accessing a certain account and they fail to carry them out for specific passed proposals as promptly or effectively as they do for others or within a reasonable time, this will be considered undermining those proposals and a cause for termination of those specific duties.

### Trial
Trial is the process to determine whether the Steering Committee or any of its members have violated the bylaws and, if necessary, take corrective action.

- To initiate a Trial, one or more individuals (the Initiators) must share a statement with the Committee.
- Initiators may be members of the Steering Committee, the public, or may even be anonymous.
- The statement must identify specific Open Tennessee Bylaws and argue (with evidence) that the Steering Committee or some of its members violated those Bylaws.
- Anyone (not excluding Committee Members or the public) may submit further arguments and/or evidence (Rebuttals) for or against the allegations in response to one or more previous (specified) statements.
- The Trial ends if 7 days pass where at least one side (for or against) has not submitted any new Rebuttals or a majority of the Committee members vote to end it.
- If any Steering Committee members co-sign any statements in favor of the allegations, the Committee shall not raise any new business (except in direct response to specific, active Trials) for 7 days.
- If the initial allegation is raised publicly, then only public Rebuttals will count in determining resolution.

### Quarter
Open Tennessee uses a calendar year for its fiscal year.
Quarters will start January through March and so forth.

## Steering Committee
### Makeup
Open Tennessee shall have a Steering Committee of at least 2 members.
Starting January 1st, 2021 and in perpetuity the proportion of Committee Members who do not belong to any legally protected class must not be higher than in the general Tennessee population with one-sided statistical significance\*.
However, if there is a change in leadership that violates this criteria, then the steering committee has 3 months to get the leadership back in compliance with this criteria.
No new grace periods are allowed before compliance has been reached from a previous grace period.

\*The significance test will be a one-sided p-value of 5% or less with the Null Hypothesis that members are randomly selected from persons within Tennessee (with replacement).

### Duties and Powers
The Steering Committee shall:
1. conduct and record an official (in person or remote) meeting with all members present at least two times each year (spanning at least 5 months)
1. ensure that the official bylaws and minutes are unambiguously linked for public access from our webpage at www.opentennessee.org
1. only amend bylaws or add Steering Committee members by [Consent](#Consent)
1. only remove Steering Committee members by resignation, [Consent](#Consent), or [Trial](#Trial)

Steering Committee Members:
1. officially [Consent](#Consent) to all active bylaws and motions when they join
1. must hand-over complete control of any accounts or resources they managed on Open Tennessee's behalf when they leave the Steering Committee
1. may resign at any time (but should make reasonable effort to give advanced notice and help find a replacement)
1. must promptly investigate any allegation or evidence they encounter of a violation or non-compliance with a bylaw
1. must initiate a [Trial](#Trial) if they have any reasonable suspicion that a bylaw was violated

[official location]: https://gitlab.com/opentennessee/administration/
