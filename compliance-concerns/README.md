# Open Tennessee Compliance Concerns
This is where we track any concerns that we may have violated our commitments,
including bylaws, steering committee decisions (in minutes) or contracts.

Steering Committee members must self-report any concerns.
But, others, including members of the public may submit claims here too.

## File Organization
Each claim should be in a separate markdown file.
Each file should be named with the date it was submitted and a brief description.
Claims should be moved according to their status (open or resolved).
